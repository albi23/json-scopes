package com.blueveery.scopes.gson.spring;

import com.blueveery.core.model.BaseEntity;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.lang.Nullable;

import java.io.Writer;
import java.lang.reflect.Type;

public class ScopedGsonHttpMessageConverter extends GsonHttpMessageConverter {

    @Override
    protected void writeInternal(Object o, @Nullable Type type, Writer writer) throws Exception {
        getGson().getAdapter(BaseEntity.class);
        super.writeInternal(o, type, writer);
    }
}
